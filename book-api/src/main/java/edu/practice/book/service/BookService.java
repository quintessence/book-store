package edu.practice.book.service;

import edu.practice.book.dao.BookDao;
import edu.practice.book.entity.Book;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class BookService {

  private final BookDao bookDao;

  @Transactional
  public Book create(Book book) {
    return bookDao.save(book);
  }

  @Transactional(readOnly = true)
  public Optional<Book> read(Long id) {
    return bookDao.findById(id);
  }

  @Transactional
  public Optional<Book> update(Long id, Book book) {
    return bookDao.findById(id)
        .flatMap(existingBook -> {
          book.setId(existingBook.getId());
          return Optional.of(bookDao.save(book));
        });
  }

  @Transactional
  public Optional<Book> delete(Long id) {
    return bookDao.findById(id)
        .flatMap(existingBook -> {
          bookDao.deleteById(id);
          return Optional.of(existingBook);
        });
  }

  @Transactional
  public Page<Book> readAll(Pageable pageable) {
    return bookDao.findAll(pageable);
  }
}
