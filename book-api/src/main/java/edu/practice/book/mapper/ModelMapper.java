package edu.practice.book.mapper;

import edu.practice.book.entity.Book;
import edu.practice.book.model.BookDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ModelMapper {

  BookDto toDto(Book book);
  Book toEntity(BookDto bookDto);
}
