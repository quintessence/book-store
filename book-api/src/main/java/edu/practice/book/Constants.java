package edu.practice.book;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

  public static final String NOT_FOUND_ERROR = "not_found";
}
