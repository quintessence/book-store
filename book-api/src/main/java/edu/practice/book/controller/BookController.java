package edu.practice.book.controller;

import edu.practice.book.entity.Book;
import edu.practice.book.exception.BookNotFoundException;
import edu.practice.book.mapper.ModelMapper;
import edu.practice.book.model.BookDto;
import edu.practice.book.model.PageDto;
import edu.practice.book.service.BookService;
import java.net.URI;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api/books")
@RequiredArgsConstructor
public class BookController {

  private final BookService bookService;
  private final ModelMapper modelMapper;

  @PostMapping
  public ResponseEntity<BookDto> create(@RequestBody @Valid BookDto book) {
    Book createdBook = bookService.create(modelMapper.toEntity(book));

    URI location = ServletUriComponentsBuilder.fromCurrentRequestUri()
        .path("/{id}")
        .buildAndExpand(createdBook.getId())
        .toUri();

    return ResponseEntity.created(location).body(modelMapper.toDto(createdBook));
  }

  @GetMapping("/{id}")
  public BookDto read(@PathVariable Long id) {
    return bookService.read(id)
        .map(modelMapper::toDto)
        .orElseThrow(() -> new BookNotFoundException(id));
  }

  @PutMapping("/{id}")
  public BookDto update(@PathVariable Long id, @RequestBody @Valid BookDto book) {
    return bookService.update(id, modelMapper.toEntity(book))
        .map(modelMapper::toDto)
        .orElseThrow(() -> new BookNotFoundException(id));
  }

  @DeleteMapping("/{id}")
  public BookDto delete(@PathVariable Long id) {
    return bookService.delete(id)
        .map(modelMapper::toDto)
        .orElseThrow(() -> new BookNotFoundException(id));
  }

  @GetMapping
  public PageDto<BookDto> getAll(Pageable pageable) {
    return PageDto.from(bookService.readAll(pageable)
        .map(modelMapper::toDto));
  }
}
