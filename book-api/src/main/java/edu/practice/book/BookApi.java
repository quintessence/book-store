package edu.practice.book;

import edu.practice.book.entity.Book;
import edu.practice.book.service.BookService;
import java.math.BigDecimal;
import java.util.Arrays;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BookApi extends SpringBootServletInitializer {

  public static void main(String[] args) {
    SpringApplication.run(BookApi.class, args);
  }

  @Bean
  public ApplicationRunner init(BookService bookService) {
    return args ->
        Arrays.asList(
            new Book(1L, "Main Kampf", new BigDecimal(1488), "Adolf", 2000),
            new Book(2L, "Букварь", new BigDecimal(228), "Олег", 500),
            new Book(3L, "Vasya Pupkin Special: Autobiography", new BigDecimal(69), "Vasya Pupkin", 800),
            new Book(4L, "Fifty shades of gray", new BigDecimal(40), "James", 514)
        ).forEach(bookService::create);
  }
}
