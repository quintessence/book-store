package edu.practice.book.exception;

public class BookNotFoundException extends NotFoundException {

  public BookNotFoundException(Long id) {
    super(String.format("Can't find the book with id [%d]", id));
  }
}
