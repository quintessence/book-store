package edu.practice.book.exception;

public class NotFoundException extends RuntimeException {

  protected NotFoundException(String message) {
    super(message);
  }
}
