package edu.practice.book.model;

import java.util.List;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class PageDto<T> {

  private Integer page;
  private Integer size;
  private Long total;
  private List<T> content;

  private PageDto(Page<T> page) {
    this.page = page.getNumber();
    this.size = page.getSize();
    this.total = page.getTotalElements();
    this.content = page.getContent();
  }

  public static <T> PageDto<T> from(Page<T> page) {
    return new PageDto<>(page);
  }
}
