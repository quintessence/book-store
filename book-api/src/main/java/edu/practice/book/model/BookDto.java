package edu.practice.book.model;

import java.math.BigDecimal;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class BookDto {

  @NotBlank
  private String name;
  @NotNull
  private BigDecimal price;
  @NotBlank
  private String author;
  @NotNull @Min(1) @Max(100)
  private Integer pages;
}
